/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => "hello world";

/**
 * problem 1 (stripPrivateProperties)
 * @param {string[]} propsToSrip properties to exlude from result objects
 * @param {Object[]} inputArray
 * @returns {Object[]} Array of objects with excluded propertues from propsToSrip
 */
exports.stripPrivateProperties = (propsToSrip, inputArray) => {
  return inputArray.reduce((accumulator, currentValue) => {
    Object.keys(currentValue)
      .filter((key) => propsToSrip.includes(key))
      .forEach((key) => delete currentValue[key]);
    accumulator.push(currentValue);
    return accumulator;
  }, []);
};

/**
 * @param {string} propName name of the property to exclude
 * @param {Object[]} inputArray
 * @returns {Object[]} Array of objects exluding objects containing propName
 */
exports.excludeByProperty = (propName, inputArray) => {
  return inputArray.filter((obj) => !Object.keys(obj).includes(propName));
};

/**
 * problem 3 (sumDeep)
 * @param {Object[]} inputArray
 */
exports.sumDeep = (inputArray) => {
  return inputArray.map((obj) => ({
    objects: obj.objects.reduce((acc, cur) => acc + cur.val, 0),
  }));
};

/**
 * @param {Object}  mappingObject
 * @param {Object[]} inputArray
 * @returns {Object[]} Array of objects where each object includes its matching status code
 */
exports.applyStatusColor = (mappingObject, inputArray) => {
  //using reduce to exlude statuses without matching color lookup
  return inputArray.reduce((accumulator, currentValue) => {
    //Use some instead of forEach to break the circuit as soon as we find a match
    Object.entries(mappingObject).some(([color, statuses]) => {
      if (statuses.includes(currentValue.status)) {
        accumulator.push({ ...currentValue, color });
        return true;
      }
    });

    return accumulator;
  }, []);
};

/**
 * Create a function
 * @param {function} greet
 * @param {string} greeting
 * @returns {string}
 */
exports.createGreeting = (greet, greeting) => (nameToGreet) =>
  greet(greeting, nameToGreet);

/**
 * @param {Object} defaults to set on a given object
 * @returns {function} Returns function which applies default properties to a given object
 */
exports.setDefaults = (defaults) => (objToApplyDefaults) => {
  Object.entries(defaults).forEach(([key, value]) => {
    if (!objToApplyDefaults.hasOwnProperty(key)) {
      objToApplyDefaults[key] = value;
    }
  });
  console.log(objToApplyDefaults);
  return objToApplyDefaults;
};

/**
 * @param {string} userName name of the user to fetch
 * @param {Object} services service instance 
 * @returns {object} return user object
 */
exports.fetchUserByNameAndUsersCompany = async (userName, services) => {
  try {
    //Users & status are orthogonal - can be fetched simultaneously
    const [users, status] = await Promise.all([
      await services.fetchUsers(),
      await services.fetchStatus(),
    ]);
    const user = users.find((user) => user.name === userName);
    const company = await services.fetchCompanyById(user.companyId);
    return { company, status, user };
  } catch (e) {
    console.log("error fetching user:", e);
  }
};
